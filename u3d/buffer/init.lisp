(import lua/basic (get-idx set-idx!))

(import data/struct ())

(defstruct (buffer mk-buffer buffer?)
  "Construct a depth buffer from the given WIDTH and HEIGHT.

   Note that this is a very trivial buffer, colours are an opaque data
   structure: it is up to the consumers of this buffer to determine how
   they are read/written.

   Future enhancements would allow generating buffers with/without depth
   support, alpha blending, RGB, etc..."
  (fields
    (immutable width
      "The width of this buffer")
    (immutable height
      "The height of this buffer")

    (immutable depths
      "The depth values for this buffer. These are stored row-first,
       so `(y - 1) * width + x` represents a value at `(x, y)`.")
    (immutable colours
      "The colour values for this buffer. These are stored row-first,
       so `(y - 1) * width + x` represents a value at `(x, y)`.")

    (immutable set-pixel!
      "The pixel setter for this buffer. Whilst this is a field, it
       should be cached as a local function and used directly.")
    (immutable clear!
      "Reset the depth buffer and fill the background with a specified
       colour."))

  (constructor new
    (lambda (width height bg)
      (assert-type! width number)
      (assert-type! height number)
      (demand (/= bg nil))

      (let [(depths {})
            (colours {})]
        (for i 1 (* width height) 1
          (set-idx! depths  i math/huge)
          (set-idx! colours i bg))

        (new
          width height
          depths colours

          (lambda (x y z colour)
            (with (idx (+ (* (- y 1) width) x))
              (when (< z (get-idx depths idx))
                (set-idx! depths  idx z)
                (set-idx! colours idx colour))))

          (lambda (bg)
            (for i 1 (* width height) 1
              (set-idx! depths  i math/huge)
              (set-idx! colours i bg))))))))
