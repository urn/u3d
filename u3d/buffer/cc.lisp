(import lua/table table)

(import u3d/buffer b)
(import u3d/buffer (buffer-set-pixel! buffer-width buffer-height) :export)

(defun mk-buffer (width height)
  "Construct a depth buffer from the given WIDTH and HEIGHT."
  (b/mk-buffer width height "f"))

(defun buffer-draw! (buffer term)
  "Draw the provided BUFFER to the given TERM object."
  (let* [(width (buffer-width buffer))
         (height (buffer-height buffer))
         (colours (b/buffer-colours buffer))

         (text-row (string/rep " " width))
         (fg-row (string/rep "0" width))]

    (for y 1 height 1
      ((.> term :setCursorPos) 1 y)
      ((.> term :blit)
        text-row fg-row
        (table/concat colours "" (+ (* (- y 1) width) 1) (* y width))))))

(defun buffer-clear! (buffer)
  "Clear the provided BUFFER object."
  ((b/buffer-clear! buffer) "f"))
