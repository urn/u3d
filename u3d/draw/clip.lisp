(import u3d/linear/vec4 (v4 m4*v4 coord))

(define-native m :hidden :syntax "-${1}")

(defun transform (matrix vector)
  "Tranform a VECTOR with the given MATRIX. This returns three objects:

    - The transformed vector

    - Whether the vector fits within the view frustrum

    - If it does not fit within the view frustru, the relevent clip
      coordinates. Otherwise, the projected vector."
  :hidden
  (let* [(trans (m4*v4 matrix vector))
         (x (coord trans :x))
         (y (coord trans :y))
         (z (coord trans :z))
         (w (* (coord trans :w) 1.0001))
         (invw (/ 1 (coord trans :w)))
         (xc 0) (yc 0) (zc 0)
         (valid true)]

    (cond
      [(< x (m w)) (set! xc -1) (set! valid false)]
      [(> x    w)  (set! xc 1)  (set! valid false)]
      [else])
    (cond
      [(< y (m w)) (set! yc -1) (set! valid false)]
      [(> y    w)  (set! yc 1)  (set! valid false)]
      [else])
    (cond
      [(< z (m w)) (set! zc -1) (set! valid false)]
      [(> z    w)  (set! zc 1)  (set! valid false)]
      [else])

    (if valid
      (values-list trans true  (list (* x invw) (* y invw) (* z invw)))
      (values-list trans false (list xc yc zc)))))

(defun interpolate (p1 p2 t)
  "Interpolate between P1 and P2 using the given T parameter. 0 will be
   equal to P1, 1 will equal to P2."
  (let* [(it (- 1 t))
         (w (+ (* (coord p1 :w) it) (* (coord p2 :w) t)))
         (iw (/ 1 w))]

    (list
      (* (+ (* (coord p1 :x) it) (* (coord p2 :x) t)) iw)
      (* (+ (* (coord p1 :y) it) (* (coord p2 :y) t)) iw)
      (* (+ (* (coord p1 :z) it) (* (coord p2 :z) t)) iw))))

(defun point (draw matrix position)
  "Draw a given point at the given POSITION, using the given view MATRIX
   to transform the point and DRAW function to display it."
  (assert-type! matrix m4)
  (assert-type! position v4)

  (with ((trans valid projected) (transform matrix position))
    (when valid (draw projected))))

(defun line (draw matrix position-1 position-2)
  "Draw a line between POSITION-1 and POSITION-2, using the given view
   matrix to transform the points and DRAW function to display it."
  (assert-type! matrix m4)
  (assert-type! position-1 v4)
  (assert-type! position-2 v4)

  (let* [((trans-1 valid-1 proj-1) (transform matrix position-1))
         ((trans-2 valid-2 proj-2) (transform matrix position-2))]

    (cond
      ;; If both positions are valid, just draw it
      [(and valid-1 valid-2) (draw proj-1 proj-2)]
      ;; If any clip point is entirely outside the bound
      [(or (and (/= (coord proj-1 :x) 0) (= (coord proj-1 :x) (coord proj-2 :x)))
           (and (/= (coord proj-1 :y) 0) (= (coord proj-1 :y) (coord proj-2 :y)))
           (and (/= (coord proj-1 :z) 0) (= (coord proj-1 :z) (coord proj-2 :z))))]

      [else
       (let* [(x1 (coord trans-1 :x))
              (y1 (coord trans-1 :y))
              (z1 (coord trans-1 :z))
              (w1 (coord trans-1 :w))

              (dx (- (coord trans-2 :x) x1))
              (dy (- (coord trans-2 :y) y1))
              (dz (- (coord trans-2 :z) z1))
              (dw (- (coord trans-2 :w) w1))

              (valid true)
              (tmin 0)
              (tmax 1)]

         ,@(map
             (lambda (bind)
               ~(when valid
                  (let ,bind
                    (cond
                      [(> denom 0)
                       (with (t (/ num denom))
                         (cond
                           [(>= t tmax) (set! valid false)]
                           [(> t tmin) (set! tmin t)]
                           [else]))]
                      [(< denom 0)
                       (with (t (/ num denom))
                         (cond
                           [(<= t tmin) (set! valid false)]
                           [(< t tmax) (set! tmax t)]
                           [else]))]
                      [(> num 0) (set! valid false)]
                      [else]))))

             '(((denom (+ dx     dw)) (num (- (m x1) w1)))
               ((denom (+ (m dx) dw)) (num (- x1     w1)))
               ((denom (+ dy     dw)) (num (- (m y1) w1)))
               ((denom (+ (m dy) dw)) (num (- y1     w1)))
               ((denom (+ dz     dw)) (num (- (m z1) w1)))
               ((denom (+ (m dz) dw)) (num (- z1     w1)))))

         (when valid
           (draw
             (interpolate trans-1 trans-2 tmin)
             (interpolate trans-1 trans-2 tmax))))])))
